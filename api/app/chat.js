const express = require('express');
const nanoid = require('nanoid');

const User = require('../models/User');
const Messages = require('../models/Messages');

const router = express.Router();

const activeConnections = {};

router.ws('/', async (ws, req) => {

    if (!req.query.token) {
        return ws.close();
    }

    const user = await User.findOne({token: req.query.token});

    if (!user) {
        return ws.close();
    }

    const id = nanoid();
    console.log('client connected! id=', id);
    activeConnections[id] = {ws, user};

    const userNames = Object.keys(activeConnections).map(connId => {
        const connection = activeConnections[connId];

        return {username: connection.user.username, id: connection.user._id};
    });
    Object.keys(activeConnections).forEach(conn => {
      activeConnections[conn].ws.send(JSON.stringify({
        type: "ACTIVE_USERS",
        userNames
      }));
    });


    ws.send(JSON.stringify({
        type: "LATEST_MESSAGES",
        messages: await Messages.find().limit(-30)
    }));

    Object.keys(activeConnections).map(connId => {
        const connection = activeConnections[connId];

        connection.ws.send(JSON.stringify({
            type: 'NEW_USER',
            author: user.username
        }))


    });


    ws.on('message', msg => {
        let decodedMessage;

        try {
            decodedMessage = JSON.parse(msg);
        } catch (e) {
            return console.log('Not a valid message');
        }

        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':
                const message = {
                    author: decodedMessage.author,
                    message: decodedMessage.message,
                    datetime: new Date().toISOString()
                };

                const messages = new Messages(message);
                messages.save();

                Object.keys(activeConnections).map(connId => {
                    const conn = activeConnections[connId];
                    conn.ws.send(JSON.stringify({
                        type: 'NEW_MESSAGE',
                        message
                    }))
                });

                break;
            default:
                console.log('Not valid message type, ', decodedMessage.type);
        }
      });



    ws.on('close', msg => {
      console.log('client disconnected id=', id);
      delete activeConnections[id];
    });

});


module.exports = router;