import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Form, Input, ListGroup, ListGroupItem} from "reactstrap";
import Moment from "react-moment";

import './Chat.css';

class Chat extends Component {
    state = {
        activeUsers: [],
        messages: [],
        message: '',
        newUser: null
    };

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/chat?token=' + this.props.user.token);


        this.websocket.onmessage = event => {
            const message = JSON.parse(event.data);

            if (message.type === 'ACTIVE_USERS') {
                this.setState({
                    activeUsers: message.userNames
                })
            }
            if (message.type === 'NEW_USER') {
                this.setState({
                    newUser: message.username
                })
            }
            if (message.type === 'LATEST_MESSAGES') {
                this.setState({
                    messages: message.messages
                })
            }
            if (message.type === 'NEW_MESSAGE') {
                this.setState({
                    messages: [
                        ...this.state.messages,
                        message.message
                    ]
                })
            }
        };
    }

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    sendMessage = event => {
        event.preventDefault();

        this.websocket.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            author: this.props.user.username,
            message: this.state.message
        }));
    };

    render() {
        return (
            <Fragment>
                <div className='ActiveUsers'>
                    {this.state.activeUsers.map(id => (
                        <ListGroup key={id.id}>
                            <ListGroupItem
                                className='ListItem'
                                style={{marginBottom: '8px'}}>{id.username}
                                <p style={{float: "right", color: 'blue'}}>Online</p>
                            </ListGroupItem>

                        </ListGroup>
                    ))}
                </div>
                <div className='Chat'>

                    {this.state.messages.map((message) => (
                        <div className="container" key={message._id}>
                            <div className="Message">
                                <div className='MessageContent'>
                                    <p><b>author: {message.author}</b></p>
                                    <p>{message.message}</p>
                                </div>
                                <div className="MessageDatetime">
                                    <Moment format="D MMM YYYY HH:mm">
                                        {message.datetime}
                                    </Moment>
                                </div>
                            </div>
                        </div>
                    ))}
                    <Form style={{position: 'absolute', bottom: '0', width: '50%', right: '15%'}}>
                        <Input
                            type="text"
                            name='message'
                            value={this.state.message}
                            onChange={this.inputChangeHandler}
                        />
                        <Button
                            onClick={this.sendMessage}
                            className='float-right'
                            style={{margin: '10px'}}>Send</Button>
                    </Form>
                </div>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

export default connect(mapStateToProps, null)(Chat);